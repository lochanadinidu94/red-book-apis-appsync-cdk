# redbook-appsync-api-graphql-cdk

##  APIs services.

* getAuth - get authentication token
* carDetails - get car details according to rego and state

### how to run deploy

```bash
# to run the app folder 
#(make sure to install aws cli beforehand)
npm run build
cdk deploy
```
### Folder structure

```bash
.
├── README.md
├── bin
│   └── red-book-apis-appsync-cdk.ts
├── cdk.json
├── cdk.out
├── jest.config.js
├── lib
│   ├── graphql-schema
│   ├── lambda-fns
│   └── red-book-apis-appsync-cdk.ts
├── node_modules
├── package.json
├── test
└── tsconfig.json
   ```

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template
