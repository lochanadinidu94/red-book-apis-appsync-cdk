import { getMakeService } from "../services/carMake.service";
const carMake = async (event: any) => {
    return getMakeService();
};

export default carMake;