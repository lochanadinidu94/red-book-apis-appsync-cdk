import { getCarDescriptionService } from "../services/carDescription.service";

const carDescription = async (rego: string, state: string) => {
  return getCarDescriptionService(rego, state);
};

export default carDescription;
