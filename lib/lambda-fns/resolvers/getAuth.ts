import { getAuthService } from "../services/getAuth.service";
const getAuth = async (event: any) => {
  return getAuthService();
};

export default getAuth;
