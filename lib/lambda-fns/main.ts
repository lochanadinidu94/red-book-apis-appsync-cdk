import getAuth from "./resolvers/getAuth";
import carDescription from "./resolvers/carDescription";
import AppSyncEvent from "./types/input.types";
import carMake from "./resolvers/carMake";

exports.handler = async (event: AppSyncEvent) => {
  switch (event.info.fieldName) {
    case "getAuth":
      return await getAuth(event);
    case "carDescription":
      return await carDescription(event.arguments.rego, event.arguments.state);
    case "carMake":
      return await carMake(event);
    default:
      return null;
  }
};
