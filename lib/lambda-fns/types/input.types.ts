type AppSyncEvent = {
  info: {
    fieldName: string;
  };
  arguments: {
    rego: string;
    state: string;
    makeCode: string;
    familyCode: String;
    year: String;
    redbookCode: String;
  };
};
export default AppSyncEvent;
