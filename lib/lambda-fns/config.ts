// getAuth config data
export const accessTokenUrl = () =>
  "https://api.satellite.iag.com.au/oauth2/v1/api/oauth2/token";
export const getAuth_client_id = () =>
  "3MVG9YDQS5WtC11pJQHW.SoB.PTHwgLx5IT3RikmiqpGzCB0kGALU4.yTj4NFqS0L1pTwd6CLc7A2QhgjuOYJ";
export const getAuth_client_secret = () =>
  "C2B2B952829090408AB26FD8C92F2DDC8093A45EB84102F767743226853C9E98";
export const getAuth_x_partner = () => "poncho";
export const getAuth_x_correlation_id = () => "AppSync";

// carDescription config data
export const redBookBaseUrl = () => "https://sit.api.satelliteinsurance.com.au";
export const carDescriptionURL = (rego: string, state: string) =>
  `${redBookBaseUrl()}/online-insurance/v1/api/registrations/${rego}/${state}`;
export const carMakeURL = () => `${redBookBaseUrl()}/online-insurance/v1/api/vehicles/makes`
export const carDescription_content_type = () => "application/json";
export const carDescription_authorization = (token:string) => `Bearer ${token}`;
export const carDescription_x_correlation_id = () => "1234";
export const carDescription_x_source_system = () => "CC";
export const carDescription_x_motor_reference = () => "RedBook"
export const carDescription_x_partner = () => "poncho"
