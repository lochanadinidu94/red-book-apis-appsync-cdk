import axios from "axios";
import {
    carDescription_authorization,
    carDescription_content_type,
    carDescription_x_correlation_id,
    carDescription_x_motor_reference,
    carDescription_x_partner,
    carDescription_x_source_system,
    carMakeURL,
} from "../config";
import { getAccessToken } from "./getAuth.service";

export const getMakeService = async () => {
    try {
        const axiosOptions = {
            headers: {
                "Content-Type": carDescription_content_type(),
                Authorization: carDescription_authorization(await getAccessToken()),
                "x-correlation-id": carDescription_x_correlation_id(),
                "x-source-system": carDescription_x_source_system(),
                "x-motor-reference": carDescription_x_motor_reference(),
                "x-partner": carDescription_x_partner(),
            },
        };
        const response = await axios.get(
            carMakeURL(),
            axiosOptions
        );
        return response.data;
    }catch (err) {
        return err
    }
}
