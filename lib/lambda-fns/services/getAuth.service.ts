import axios from "axios";
import {
  accessTokenUrl,
  getAuth_client_id,
  getAuth_client_secret,
  getAuth_x_partner,
  getAuth_x_correlation_id,
} from "../config";

const axiosOptions = {
  headers: {
    client_id: getAuth_client_id(),
    client_secret: getAuth_client_secret(),
    "x-partner": getAuth_x_partner(),
    "x-correlation-id": getAuth_x_correlation_id(),
  },
};
export const getAuthService = async () => {
  try {
    const response = await axios.get(accessTokenUrl(), axiosOptions);
    return response.data;
  } catch (e) {
    return e;
  }
};

export const getAccessToken = async () => {
  try {
    const response = await axios.get(accessTokenUrl(), axiosOptions);
    return response.data.accessToken;
  } catch (e) {
    return e;
  }
};
