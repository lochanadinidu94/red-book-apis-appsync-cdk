import * as lambda from "aws-cdk-lib/aws-lambda";
import { Construct } from "constructs";
import { Duration, Expiration, Stack, StackProps } from "aws-cdk-lib";
import {
  AuthorizationType,
  GraphqlApi,
  SchemaFile,
} from "aws-cdk-lib/aws-appsync";

export class RedBookApisAppsyncCdk extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const api = new GraphqlApi(this, "Api", {
      name: "test-redbook-api-appsync-cdk",
      schema: SchemaFile.fromAsset("lib/graphql-schema/redbookSchema.graphql"),
      authorizationConfig: {
        defaultAuthorization: {
          authorizationType: AuthorizationType.API_KEY,
          apiKeyConfig: {
            expires: Expiration.after(Duration.days(365)),
          },
        },
      },
      xrayEnabled: true,
    });

    const carLambda = new lambda.Function(this, "AppSyncCarHandler", {
      runtime: lambda.Runtime.NODEJS_12_X,
      handler: "main.handler",
      code: lambda.Code.fromAsset("lib/lambda-fns"),
      memorySize: 1024,
    });

    const lambdaDs = api.addLambdaDataSource("lambdaDatasource", carLambda);

    lambdaDs.createResolver("getAuth", {
      typeName: "Query",
      fieldName: "getAuth",
    });

    lambdaDs.createResolver("carDescription", {
      typeName: "Query",
      fieldName: "carDescription",
    });

    lambdaDs.createResolver("carMake", {
      typeName: "Query",
      fieldName: "carMake",
    });

  }
}
